cmake_minimum_required(VERSION 3.14)

#-------------------------------------------------------------------------------
# Initialize
#-------------------------------------------------------------------------------
project(quarkdb)
set(CMAKE_DISABLE_IN_SOURCE_BUILD ON)
set(CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake)

if (CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
  set(CMAKE_INSTALL_PREFIX "/usr" CACHE PATH "Default install prefix: /usr" FORCE)
endif ()

include(GNUInstallDirs)

#-------------------------------------------------------------------------------
# Determine if QuarkDB is built as a subproject (using add_subdirectory)
# or if it is the master project.
#-------------------------------------------------------------------------------
set(MASTER_PROJECT OFF)
if (CMAKE_CURRENT_SOURCE_DIR STREQUAL CMAKE_SOURCE_DIR)
  set(MASTER_PROJECT ON)
endif ()


if (NOT PACKAGEONLY)
  include(CompileFlags)
endif()

#-------------------------------------------------------------------------------
# Export cmake compile commands by default
#-------------------------------------------------------------------------------
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

#-------------------------------------------------------------------------------
# Make gtest / gmock available for all downstream CMakeLists.txt that need it
#-------------------------------------------------------------------------------
if (MASTER_PROJECT)
  set(GTEST "${CMAKE_SOURCE_DIR}/deps/googletest/")
  set(GTEST_BINARY_DIR "${CMAKE_BINARY_DIR}/deps/googletest/")
  add_subdirectory("${GTEST}" "${GTEST_BINARY_DIR}")
  # Add alias libraries to emulate same behavior as external GoogleTest
  add_library(GTest::GTest ALIAS gtest)
  add_library(GTest::Main ALIAS gtest_main)
  add_library(GTest::gmock ALIAS gmock)
  add_library(GTest::gmock_main ALIAS gmock_main)
  message(STATUS "Handled GTest find in QuarkDB")
endif()

#-------------------------------------------------------------------------------
# Activate include-what-you-use
#-------------------------------------------------------------------------------
option(ENABLE_IWYU "Enable include-what-you-use tool" OFF)

if(ENABLE_IWYU)
  find_program(IWYU_PATH NAMES include-what-you-use iwyu)
  if(NOT IWYU_PATH)
    message(FATAL_ERROR "Could not find include-what-you-use")
  endif()

  set(CMAKE_CXX_INCLUDE_WHAT_YOU_USE ${IWYU_PATH})
  set(CMAKE_C_INCLUDE_WHAT_YOU_USE ${IWYU_PATH})
endif()

#-------------------------------------------------------------------------------
# Regenerate Version.hh
#-------------------------------------------------------------------------------
add_custom_target(GenerateQdbVersionInfo ALL DEPENDS src/Version.hh)
add_custom_command(
  OUTPUT src/Version.hh
  COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/genversion.py --template src/Version.hh.in --out src/Version.hh --eos-version \"${VERSION}\"
  WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
)

#-------------------------------------------------------------------------------
# Are we building with ThreadSanitizer?
#-------------------------------------------------------------------------------
message("${CMAKE_CXX_FLAGS}")
if("${CMAKE_CXX_FLAGS}" MATCHES -fsanitize=thread)
    set(BUILDING_WITH_TSAN TRUE)
else()
    set(BUILDING_WITH_TSAN FALSE)
endif()

#-------------------------------------------------------------------------------
# Link the xrootd library with jemalloc?
#-------------------------------------------------------------------------------
option(XROOTD_JEMALLOC "Link xrootd library with jemalloc?" OFF)

#-------------------------------------------------------------------------------
# Search for dependencies
#-------------------------------------------------------------------------------
option(PACKAGEONLY "Build without dependencies" OFF)

if(NOT PACKAGEONLY)
  find_package(Threads REQUIRED)
  find_package(RocksDB REQUIRED)
  find_package(XRootD REQUIRED)
  find_package(uuid REQUIRED)
  find_package(BZip2 REQUIRED)
  find_package(zstd REQUIRED)
  find_package(lz4 REQUIRED)
  find_package(zlib REQUIRED)
  find_package(openssl REQUIRED)
  find_package(glibc REQUIRED)
  # Required for backward-cpp libdw support in src/utils/Stacktrace.hh
  find_package(elfutils REQUIRED)
endif()

#-------------------------------------------------------------------------------
# Check if we're generating a test coverage report
#-------------------------------------------------------------------------------
option(TESTCOVERAGE "Enable support for tracking test coverage" OFF)

if(TESTCOVERAGE)
  set(GCOV_CFLAGS "-fprofile-arcs -ftest-coverage --coverage")
  set(GCOV_LIBS "gcov")

  add_custom_target(
    raw-test-trace
    COMMAND lcov --capture --base-directory ${CMAKE_CURRENT_SOURCE_DIR} --directory ${CMAKE_BINARY_DIR} --output-file ${CMAKE_BINARY_DIR}/raw-trace.info
  )

  add_custom_target(
    filtered-test-trace
    COMMAND lcov --extract ${CMAKE_BINARY_DIR}/raw-trace.info "${CMAKE_CURRENT_SOURCE_DIR}/src/\\*" --output-file ${CMAKE_BINARY_DIR}/filtered-trace.info
    DEPENDS raw-test-trace
  )

  add_custom_target(
    coverage-report
    COMMAND genhtml ${CMAKE_BINARY_DIR}/filtered-trace.info --output-directory ${CMAKE_BINARY_DIR}/coverage-report
    DEPENDS filtered-test-trace
  )
endif()

#-------------------------------------------------------------------------------
# Use ccache, if available
#-------------------------------------------------------------------------------
find_program(CCACHE_PROGRAM ccache)
if(CCACHE_PROGRAM)
    set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE "${CCACHE_PROGRAM}")
    set_property(GLOBAL PROPERTY RULE_LAUNCH_LINK "${CCACHE_PROGRAM}")
endif()

#-------------------------------------------------------------------------------
# Compiler options
#-------------------------------------------------------------------------------
add_definitions(-Wall -Wextra -Wno-unused-parameter -std=c++17 -g -fPIC -DASIO_STANDALONE)

#-------------------------------------------------------------------------------
# Build source and tests
#-------------------------------------------------------------------------------
if(NOT PACKAGEONLY)
  add_subdirectory(deps/backward-cpp)
  include(deps/backward-cpp/BackwardConfig.cmake)

  include_directories(
    ${CMAKE_CURRENT_SOURCE_DIR}/src
    ${CMAKE_CURRENT_SOURCE_DIR}/deps
    ${CMAKE_CURRENT_SOURCE_DIR}/deps/CLI11/include
    ${CMAKE_CURRENT_SOURCE_DIR}/deps/asio/asio/include)


  if (MASTER_PROJECT)
    add_subdirectory(deps/qclient)
  endif()

  add_subdirectory(src)
  add_subdirectory(test)
  add_subdirectory(tools)
endif()

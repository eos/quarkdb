#!/usr/bin/env bash
set -e

##------------------------------------------------------------------------------
## Bootstrap packages - needed for 'builddep'
##------------------------------------------------------------------------------
dnf install -y expect gcc-c++ make rpm-build which git dnf-utils libtsan dnf-plugins-core

##------------------------------------------------------------------------------
## Extract quarkdb build dependencies from its specfile.
##------------------------------------------------------------------------------
./packaging/make-srpm.sh
dnf builddep -y build/SRPMS/*


#-------------------------------------------------------------------------------
# Require c++17
#-------------------------------------------------------------------------------
include(CheckCXXCompilerFlag)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
# Avoid having CMAKE treat include directories on imported libraries as systems
# includes. In newer gcc versions the systems includes are added using the
# "-isystem" flag instead of "-I". This currently breaks the build on Fedora 36
# and 37.
set(CMAKE_NO_SYSTEM_FROM_IMPORTED TRUE)
check_cxx_compiler_flag(-std=c++17 HAVE_FLAG_STD_CXX17)

if(NOT HAVE_FLAG_STD_CXX17)
  message(FATAL_ERROR "A compiler with -std=c++17 support is required.")
endif()

#-------------------------------------------------------------------------------
# Sanitizer flags
#-------------------------------------------------------------------------------
if (ASAN)
  # Copy CMAKE_CXX_FLAGS and unset them to avoid false negatives when checking
  # for support of the various flags
  set(SAVE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")
  unset(CMAKE_CXX_FLAGS)
  set(CMAKE_REQUIRED_FLAGS "-fsanitize=address")
  check_cxx_compiler_flag(-fsanitize=address HAVE_FLAG_ASAN)
  unset(CMAKE_REQUIRED_FLAGS)
  set(CMAKE_CXX_FLAGS "${SAVE_CXX_FLAGS}")

  if (NOT HAVE_FLAG_ASAN)
    message(FATAL_ERROR "A compiler with '-fsanitize=address' support is required.")
  endif()

  message(STATUS "Enabling ASAN FLAGS")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fsanitize=address")
endif()

if (TSAN)
  # Copy CMAKE_CXX_FLAGS and unset them to avoid false negatives when checking
  # for support of the various flags
  set(SAVE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")
  unset(CMAKE_CXX_FLAGS)
  set(CMAKE_REQUIRED_FLAGS "-fsanitize=thread")
  check_cxx_compiler_flag(-fsanitize=thread HAVE_FLAG_TSAN)
  unset(CMAKE_REQUIRED_FLAGS)
  set(CMAKE_CXX_FLAGS "${SAVE_CXX_FLAGS}")

  if (NOT HAVE_FLAG_TSAN)
    message(FATAL_ERROR "A compiler with '-fsanitize=thread' support is required.")
  endif()

  message(STATUS "Enabling TSAN FLAGS")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fsanitize=thread")
endif()

# Try to find elfutils
# Once done, this will define
#
# ELFUTILS_FOUND          - system has elfutils
# ELFUTILS_INCLUDE_DIRS   - zlib include directories
# ELFUTILS_LIBRARIES      - libraries needed to use elfutils
#
# and the following imported targets
#
# ELFUTILS::DW
#

find_path(ELFUTILS_INCLUDE_DIR
  NAMES elfutils/libdw.h elfutils/libdbfl.h
  PATHS ${ELFUTILS_ROOT})

find_library(ELFUTILS_LIBRARY
  NAME dw
  HINTS ${ELFUTILS_ROOT}
  PATH_SUFFIXES ${CMAKE_INSTALL_LIBDIR})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(elfutils
  REQUIRED_VARS ELFUTILS_LIBRARY ELFUTILS_INCLUDE_DIR)

mark_as_advanced(ELFUTILS_FOUND ELFUTILS_INCLUDE_DIR ELFUTILS_LIBRARY)

if (ELFUTILS_FOUND AND NOT TARGET ELFUTILS::DW)
  add_library(ELFUTILS::DW UNKNOWN IMPORTED)
  set_target_properties(ELFUTILS::DW PROPERTIES
    IMPORTED_LOCATION "${ELFUTILS_LIBRARY}"
    INTERFACE_INCLUDE_DIRECTORIES "${ELFUTILS_INCLUDE_DIR}")
endif()

unset(ELFUTILS_INCLUDE_DIR)
unset(ELFUTILS_LIBRARY)

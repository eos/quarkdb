macro(buildRocksDB)
  include(ExternalProject)
  include(CheckCXXCompilerFlag)
  check_cxx_compiler_flag(-Wno-error=deprecated-copy COMPILER_WARNS_DEPRECATED_COPY)
  check_cxx_compiler_flag(-Wno-error=pessimizing-move COMPILER_WARNS_PESSIMIZING_MOVE)
  set(EXTRA_OPTIONS " ")

  if(COMPILER_WARNS_DEPRECATED_COPY)
    set(EXTRA_OPTIONS " ${EXTRA_OPTIONS} -Wno-error=deprecated-copy")
  endif()

  if(COMPILER_WARNS_PESSIMIZING_MOVE)
    set(EXTRA_OPTIONS " ${EXTRA_OPTIONS} -Wno-error=pessimizing-move")
  endif()

  ExternalProject_Add(BuildRocksDB
    URL "${CMAKE_SOURCE_DIR}/deps/rocksdb"
    PREFIX "${CMAKE_BINARY_DIR}/deps/rocksdb"
    CONFIGURE_COMMAND ""
    BUILD_IN_SOURCE 1
    BUILD_BYPRODUCTS "${CMAKE_BINARY_DIR}/deps/rocksdb/src/BuildRocksDB/librocksdb.a"
    BUILD_COMMAND bash -c "export PORTABLE=1 && export DISABLE_JEMALLOC=1 && export OPT='-fPIC -DNDEBUG -O3 ${EXTRA_OPTIONS} ' && make $(rpm --eval %{?_smp_mflags}) static_lib tools_lib USE_RTTI=1 DEBUG_LEVEL=0"
    INSTALL_COMMAND "")

  ExternalProject_Get_Property(BuildRocksDB source_dir)
  set(ROCKSDB_INCLUDE_DIR ${source_dir}/include)
  ExternalProject_Get_Property(BuildRocksDB binary_dir)
  set(ROCKSDB_LIBRARY ${binary_dir}/librocksdb.a)
  file(MAKE_DIRECTORY ${binary_dir})
  file(MAKE_DIRECTORY ${ROCKSDB_INCLUDE_DIR})
  find_package_handle_standard_args(RocksDB
    REQUIRE_VARS ROCKSDB_LIBRARY ROCKSDB_INCLUDE_DIR)

  if (ROCKSDB_FOUND AND NOT TARGET ROCKSDB::ROCKSDB)
    add_library(ROCKSDB::ROCKSDB STATIC IMPORTED)
    add_dependencies(ROCKSDB::ROCKSDB BuildRocksDB)
    set_target_properties(ROCKSDB::ROCKSDB PROPERTIES
          IMPORTED_LOCATION "${ROCKSDB_LIBRARY}"
          INTERFACE_INCLUDE_DIRECTORIES "${ROCKSDB_INCLUDE_DIR}"
          INTERFACE_COMPILE_DEFINITIONS "HAVE_ROCKSDB=1")
  endif()
endmacro()

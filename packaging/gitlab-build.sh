#!/usr/bin/env bash
set -e

git submodule update --init --recursive
./packaging/make-srpm.sh

if which dnf; then
  dnf builddep -y build/SRPMS/*
else
  yum-builddep -y build/SRPMS/*
fi

rpmbuild --rebuild --with server --define "_build_name_fmt %%{NAME}-%%{VERSION}-%%{RELEASE}.%%{ARCH}.rpm" build/SRPMS/*

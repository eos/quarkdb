#!/usr/bin/env bash
set -e

#-------------------------------------------------------------------------------
# Generate a source RPM - run this from the root fo the git repository.
#-------------------------------------------------------------------------------
VERSION_FULL=$(./genversion.py --template-string "@VERSION_FULL@")
printf "Version: ${VERSION_FULL} for srpm\n"
mkdir -p build
./packaging/make-dist.sh
TARBALL="quarkdb-${VERSION_FULL}.tar.gz"
BUILD_DIR=$PWD/build
pushd build
echo "INFO: Creating the srpms"
rpmbuild --define "_source_filedigest_algorithm md5" --define "_binary_filedigest_algorithm md5" -ts ${TARBALL} --define "_topdir ${BUILD_DIR}" --with server
popd
